#!/usr/bin/env python

#title             : Quine–McCluskey algorithm
#module            : main
#python_version    : 3.4
#required_packages : colorama 0.3

def pach(i, j):
    pached = implicant(j.N)
    pached.P = i.P + i.N - j.N
    return pached

class implicant:
    def __init__(self, Num):
        self.N = Num
        self.Ind = bin(Num).count('1')
        self.P = 0
        self.Pw = False

    def __eq__(self, other):
        return self.N == other.N\
           and self.Ind == other.Ind\
           and self.P == other.P

    def __hash__(self):
        return hash(('N', self.N, 'Ind', self.Ind, 'P', self.P))

def isPowerOf2(num): # bit magic
    return (num & (num - 1) == 0) and (num > 0)

def printStep(implArray):
    for i in implArray:
        print("%3d" % i.N, end='')

    print("")

    for i in implArray:
        print("%3d" % i.Ind, end='')

    print("")

    for i in implArray:
        print("%3d" % i.P, end='')

    print("\n")

def MDNFoutput(implArray):
    outStr = ""
    for i in implArray:
        P = '{0:04b}'.format(i.P)
        N = '{0:04b}'.format(i.N)
        O = ""

        for j in range(len(P)): 
            if P[j] == "1": O += "-"
            elif N[j] == "1": O += "1"
            else: O += "0"
            outStr += O
            O = ""

        outStr += " V "
    print(outStr[:-3])

def finalize(checkoutTable, implsArray): 
    # Todo: rewrite this function
    finalTable = [[0 for x in range(0, len(checkoutTable))]\
                            for x in range(0, len(implsArray))]

    for l, N in enumerate(checkoutTable):
        for k, j in enumerate(implsArray):
            if (N & ~j.P) == j.N:
                finalTable[k][l] = 1

    finalTableSum = [0 for x in range(0, len(checkoutTable))]
    sumCol = 0
    for i in range(len(checkoutTable)):
        for j in range(len(implsArray)):
            sumCol += finalTable[j][i]
        finalTableSum[i] = sumCol
        sumCol = 0

    for j in range(len(checkoutTable)):
        for i in range(len(implsArray)):
            finalTableSum[j] -= finalTable[i][j]
        if 0 not in finalTableSum: implsArray.remove(j)

    return implsArray

def main():
    ImplsArrayNext = []
    ImplsArrayCurr = []
    ImplsArrayTDNF = []
    MDNFcheckouttable = []

    var = int(input("Вариант по списку: "))
    binstr = '{0:05b}'.format(var)
    source = "1111101-11-" + binstr[:5]
    # source = input("Условие: ")
    print(source)
    print("")

    for i in range(len(source)):
        if source[i] == '1' or source[i] == '-':
            ImplsArrayCurr.append(implicant(i))
            MDNFcheckouttable.append(i)

    while True:
        for i in ImplsArrayCurr:
            for j in ImplsArrayCurr:
                if i.N > j.N and i.Ind > j.Ind:
                    if (i.Ind - j.Ind) == 1 and isPowerOf2(i.N - j.N):
                        if i.P == j.P:
                            ImplsArrayNext.append(pach(i, j))
                            i.Pw = True
                            j.Pw = True

        if not ImplsArrayNext:
            ImplsArrayTDNF += ImplsArrayCurr
            print("ТДНФ:")
            printStep(set(ImplsArrayTDNF))
            ImplsArrayTDNF = finalize(MDNFcheckouttable, set(ImplsArrayTDNF))
            MDNFoutput(set(ImplsArrayTDNF))
            break

        for i in ImplsArrayCurr:
            if i.Pw == False:
                ImplsArrayTDNF.append(i)

        ImplsArrayCurr = []
        ImplsArrayCurr = list(ImplsArrayNext)
        printStep(set(ImplsArrayCurr))
        ImplsArrayNext = []

main()
