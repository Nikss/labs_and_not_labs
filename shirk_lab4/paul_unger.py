#!/usr/bin/env python

#title             : Paul Unger algorithm
#author            : Ливенцев Е.В.
#module            : main
#python_version    : 3.4
#required_packages : none

def print_op(a, b, res, type_, sign):
    a_ = list(a); b_ = list(b); res_ = list(res)
    pr_ = [a_, b_, res_]
    for k in range(len(pr_)):
        list_ = pr_[k]
        for i in range(len(list_)):
            if type(list_[i]) is int: list_[i] += 1
            if type(list_[i]) is list:
                list_[i] = [x+1 for x in list_[i]]

    print(type_, pr_[0], '{:^3}'.format(sign),\
            pr_[1], '{:^3}'.format('='), pr_[2])

def is_compatible(g_table, col_num_1, col_num_2):
    "Detects if outputs are compatible"
    for x in g_table:
        # Если оба определены и не равны - несовместимость
        if (x[col_num_1] >= 0 and x[col_num_2] >= 0 and x[col_num_1] != x[col_num_2]):
            return False
    return True

def calc_new_states(f_table, col_num_1, col_num_2):
    "Returns new states"
    new_states = []
    for x in f_table:
        if (x[col_num_1] >= 0 and x[col_num_2] >= 0\
          and x[col_num_1] != x[col_num_2]):

            to_add = [x[col_num_1],x[col_num_2]]
            main_state_pair = [col_num_1, col_num_2]
            main_state_pair.sort()
            to_add.sort()
            not_in_list = True
            for y in new_states:
                if to_add == y:
                    not_in_list = False    
            if not_in_list and to_add != main_state_pair:
                new_states.append(to_add)

    return new_states

def union(a,b):
    "Unites lists"
    res = a
    for x in b:
        if not x in res:
            res.append(x)
    print_op(a, b, res, "union", "U")
    return res

def intersect(a, b,\
    suppress_ouput=False):
    "Intersects lists"

    res = []
    for x in a:
        if x in b:
            res.append(x)
    if not suppress_ouput:
        print_op(a, b, res, "intersect", "^")
    return res

def eq(a,b):
    "Checks lists for eq or (b in a)"
    if len(a) != len(b):
        for x in b:
            if not x in a:
                return False
        return True
    a.sort()
    b.sort()
    for i in range(len(a)):
        if a[i] != b[i]:
            return False
    return True

def list_del_same(a):
    "Deletes repeats in a list"
    res = []
    if a == []:
        return res
    res.append(a[0])
    for x in a:
        suppress_ouput = True
        if intersect([x], res,\
            suppress_ouput) == []:
            res.append(x)
    return res

#==============================================================================

def load_table(file_):
    with open(file_) as f:
        raw_table = f.readlines()

    table = []
    print('')
    print(file_ + ": ")
    for line in raw_table:
        table.append([])
        line = line.rstrip()
        print(line)
        # tmp = line.split()
        tmp = list(line)
        for x in tmp:
            if x == "-":
                tmp[tmp.index(x)] = "-1"
        for y in tmp:
            table[len(table)-1].append(int(y)-1)
    return table

def print_table(table_, only_x=False):
    t = list(table_)
    
    for i in range(len(t)):
        t[i] = list(reversed(t[i]))

    align = len(t[0]) + 1
    t.append([[]]*align)
    for i in range(len(t)):
        t[i] += [[]]*(align - len(t[i]))

    table = [ [ 0 for x in range(0, len(t[0]))]\
                            for x in range(0, len(t))]

    for i in range(len(t)):
        for j in range(len(t[0])):
            table[i][j] = t[j][len(t[0])-1-i]

    print('')
    for line in table:
        for cell in line:
            if not only_x:
                if cell == []: print('{:^5}'.format('-'), end='')
                elif cell == [-1]: print('{:^5}'.format('x'), end='')
                else:
                    str_ = ''
                    for list_ in cell: 
                        for digit in list_:
                            str_ += str(digit + 1)
                    print('{:^5}'.format(str_), end='')
            else:
                if cell == [-1]: print('{:^5}'.format('x'), end='')
                else: print('{:^5}'.format('-'), end='')
        print('')
    print('')

def main():
    f = load_table("f.txt")
    g = load_table("g.txt")

    # I. Заполнение таблицы предварительной совместимости
    comp_table = []
    for i in range(len(f[0]) - 1):
        comp_table.append([])
        for j in range(i + 1, len(f[0]) ):
            if is_compatible(g, i, j):
                comp_table[i].append(calc_new_states(f, i, j))
            else:
                comp_table[i].append([-1])

    print_table(comp_table)

    # II. Формирование таблицы полной несовместимости
    not_cmp_list = []
    not_in_list = True
    not_cmp = []
    one_added = True # Чтобы войти в цикл

    while one_added:
        one_added = False

        # Выбираем нераспространенные несовместимые состояния
        # break обоих циклов, если добавилась новая пара несовместимых состояний
        for i in range(len(comp_table)):
            for j in range(len(comp_table[i])):
                if comp_table[i][j] == [-1]:
                    not_cmp = [i,j+1+i]
                    not_cmp.sort()
                    not_in_list = True
                    for x in not_cmp_list:
                        if not_cmp == x:
                            not_in_list = False
                    if not_in_list:
                        one_added = True
                        not_cmp_list.append(not_cmp)
                        break
            if one_added: break

        # Распространяем их
        for i in range(len(comp_table)):
            for j in range(len(comp_table[i])):
                for x in comp_table[i][j]:
                    if x == not_cmp:
                        comp_table[i][j] = [-1]

    # Для каждого состояния составляем список совместимых состояний
    S = []
    for i in range(len(comp_table)):
        S.append([])
        for j in range(len(comp_table[i])):
            if comp_table[i][j] != [-1]:
                S[i].append(j+1+i)
    only_x = True
    print_table(comp_table, only_x)

    def increment_L(L):
        ans = []
        for x in L:
            x.sort()
            ans.append([])
            for y in x:
                ans[len(ans)-1].append(y+1)
        return ans

    # III. Формируем множество максимальных классов совместимости L
    L = []
    L.append([len(f[0])-1])
    S.reverse()
    for i in range(len(f[0])-1):
        tmp =[]
        tmp2=[]
        to_del = []
        for x in L:
            tmp.append(intersect(x,S[i]))
        for x in tmp:
            tmp2.append(union(x,[len(f[0])-i-2]))
        print('L-', end='')
        L = union(L,tmp2)

        for x in L:
            for y in L:
                if L.index(x) != L.index(y):
                    if eq(y,x):
                        to_del.append(L.index(x))

        to_del = list_del_same(to_del)
        to_del.sort(reverse=True)
        for x in to_del:
            del L[x]
        print("\nZ" + str(len(f[0])-1-i),\
            ": L =", increment_L(L), end='\n\n')

    print('')
    print('Finally:')
    print("L =", increment_L(L))
    print('')
    print("End.")

    exit()

main()
