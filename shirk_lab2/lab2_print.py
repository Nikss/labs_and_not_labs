#!/usr/bin/env python

#title             : TA Lab 2
#module            : console output
#python_version    : 3.4
#required_packages : colorama 0.3

from lab2_const import *

def by_step_input():
    while(True):
        print('')
        c = input(STR_ENTER_CTRL).lower()

        if c == INPUT_QUIT: break

        if not_allowed_input(c):
            print(STR_WRONG_INPUT)
            continue
        else:
            c = input_letters_dict(c)
            break
    return c

def print_pic():
    print('')
    print(STR_HDR_SCHEME)

    pic = scheme()

    for c in scheme_chars():
        pic = pic.replace(c, highlight(c) + c + RESET_COLOR)

    print(pic)
    print(RESET_COLOR, end='')

def separator(length): return '-'*length

def print_cfg(table):
    print('')
    print(STR_HDR_TRTABLE)
    
    header = STR_TRTABLE_HEAD

    for i in range(0, len(table[0])):
        header += str(i) + ' '
    header = header[:-1]

    sep_ = separator((len(header) - 2*len(RESET_COLOR)))

    print(sep_)
    print(header)
    print(sep_)

    for r, line in enumerate(table):
        print(RESET_COLOR, end='')
        print(FORMAT_CFG_L_COL.format(r), end='')

        print(COLOR_MAGENTA, end='')
        for i in range(0, len(line)):
            print('{:2}'.format(line[i]), end='')
        print('')

    print(RESET_COLOR)

def print_com_sequence(com_sequence):
    if com_sequence == '':
        exit_(STR_EMPTY_COMMSEQ)

    com_sequence = com_sequence[:-1] # rem '\n'

    for c in com_sequence:
        if not_allowed_input(c):
            exit_(STR_WRONG_INPUT)

    print(STR_HDR_COMMSEQ)    

    split_sequence = list(com_sequence[0 + i:COMSEQ_OUTLEN + i]\
        for i in range(0, len(com_sequence), COMSEQ_OUTLEN))

    print(separator(COMSEQ_OUTLEN*4)[:-1], end='')

    left = 0
    right = COMSEQ_OUTLEN
    for i in range(0, len(split_sequence)):
        print(RESET_COLOR)
        for k in range(left, right):
            print(FORMAT_COMSEQ_IND.format(k), end=' ')
        if i + 1 < len(split_sequence):
            left += COMSEQ_OUTLEN
            right += len(split_sequence[i + 1])

        print('')
        for c in split_sequence[i]:
            c = input_letters_dict(c)
            COLOR = COLOR_MAGENTA if c == '0' else COLOR_CYAN
            print(COLOR + FORMAT_COMSEQ.format(int(c)), end=' ')

    print(RESET_COLOR)
    print('')

def print_out_hdr():
    header = STR_OUTTABLE_HEAD
    sep_ = separator(len(header))

    print(STR_HDR_OUTPUT)

    print(sep_)
    print(header)
    print(sep_)

def print_tick(no, command, controls, output):
    none = '|'

    out_ =  RESET_COLOR
    out_ += FORMAT_TICK_L_COL.format(no)

    out_ += COLOR_CYAN
    command = command.replace('0', '-')
    out_ += command + ' '

    out_ += COLOR_MAGENTA

    for i, ctrl in enumerate(controls):
        out_ += (signal_chars[i] if ctrl.is_on() else none) + ' '

    out_ += highlight(output[0])
    out_ += output[0] + ' '

    out_ += highlight(output[1])
    out_ += output[1] + ' '

    out_ += RESET_COLOR

    print(out_)
