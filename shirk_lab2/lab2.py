#!/usr/bin/env python

#title             : TA Lab 2
#module            : main
#python_version    : 3.4
#required_packages : colorama 0.3

'''
usage: "lab2.py input.txt trans_table.txt",
       where "input.txt" - line of commands (0, 2, 4 or 6);
       "trans_table.txt" - transition table, size: (number of commands) x (number of states).

       or "lab2.py --by_step trans_table.txt" (interactive mode)
       then available commands is 'b' (button), 's' (sensor), 'bs' (both), 
       Enter to skip command, 'q' for exit.
'''

import sys
import colorama

from lab2_const import *
from lab2_print import *
from moore_fsm_ import *

class signal():
    def __init__(self, limit):
        self._counter = 0
        self.limit = limit
        self._level = False

    def set_active(self):
        self._level = True

    def _reset(self):
        self._level = False
        self._counter = 0

    def is_on(self):
        return self._level

    def tick(self):
        if self._level == True and\
          self._counter < self.limit:
            self._level = True
            self._counter += 1

        if self._counter == self.limit:
            self._reset()

def checkout_trans_table(trans_table):
    if len(trans_table) < MAX_COMMANDS\
        or trans_table[0][0] == '':
        exit_(STR_WRONG_TRANS_TABLE)

    for i in range(len(trans_table)):
        trans_table[i] = tt_check_line(trans_table[i])

    len_0 = len(trans_table[0])
    if len_0 < STATES_NUM: 
        exit_(STR_WRONG_TRANS_TABLE)

    for i in range(len(trans_table)):
        len_i = len(trans_table[i])
        
        if len_i > len_0:
            trans_table[i] = trans_table[i][:len_0]
        elif len_i < len_0:
            trans_table[i] += '-'*(len_0 - len_i)

def load_cfg(table_txt):  
    with open(table_txt) as f: 
        trans_table = f.readlines()
    
    checkout_trans_table(trans_table)
    print_pic()
    print_cfg(trans_table) 

    cfg = {}
    cfg['outputs'] = outputs()
    cfg['transitions'] = trans_table

    return cfg

def exec_(n, controls, state_machine, c, by_step_flag):
    command = '{0:03b}'.format(int(c))

    for i, ctrl in enumerate(controls):
        if command[i] == '1': ctrl.set_active()
        ctrl.tick()

        if i == len(controls) - 2: break

    impact = ''
    for ctrl in controls:
        impact += '1' if ctrl.is_on() else '0'
   
    if not by_step_flag:
        print_tick(n, command, controls,\
            state_machine.get_output())

    state_machine.change_state(int(impact, 2))

    if is_output_w_delay(state_machine.get_output()):
        controls[2].set_active()
    controls[2].tick()

    if by_step_flag:
        print_tick(n, command, controls,\
            state_machine.get_output())

def main():
    cfg = load_cfg(sys.argv[2])
    crossroad = moore_state_machine(cfg)

    button = signal(BUTTON_DELAY)
    sensor = signal(SENSOR_DELAY)
    holdsg = signal(HOLDSG_DELAY)

    controls = [button, sensor, holdsg]

    if sys.argv[1] == ARG_BYSTEP:
        print_out_hdr()
        by_step = True

        n = 0
        while(True):
            command = by_step_input() if n != 0 else 0

            if command == INPUT_QUIT: break
            exec_(n, controls, crossroad, command, by_step)

            if n == 0: print_by_step_help()
            n += 1
    else:
        with open(sys.argv[1]) as f:
            com_sequence = f.readline()

        print_com_sequence(com_sequence)
        print_out_hdr()

        by_step = False
        for n, c in enumerate(com_sequence):
            if c == '\n': break
            
            if not_allowed_input(c):
                exit_(STR_WRONG_INPUT)
            c = input_letters_dict(c)
            
            exec_(n, controls, crossroad, c, by_step)

    exit_(STR_END)

colorama.init()
main()
