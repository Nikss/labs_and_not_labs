#!/usr/bin/env python

#title             : TA Lab 2
#module            : defines
#python_version    : 3.4
#required_packages : colorama 0.3

from colorama import Fore

COMSEQ_OUTLEN = 16

MAX_COMMANDS  = 8
STATES_NUM    = 6

BUTTON_DELAY  = 6
SENSOR_DELAY  = 4
HOLDSG_DELAY  = 2

def exit_(str_):
    print(str_)
    exit()

def outputs():
    return {
        '000':'Gr',
        '001':'Yr',
        '010':'Yr',
        '011':'Rg',
        '100':'Ry',
        '101':'Ry'
    }

def is_output_w_delay(o):
    return o in ['Yr', 'Ry']

COLOR_BLUE    = Fore.BLUE
COLOR_RED     = Fore.RED
COLOR_GREEN   = Fore.GREEN
COLOR_MAGENTA = Fore.MAGENTA
COLOR_CYAN    = Fore.CYAN
COLOR_YELLOW  = Fore.YELLOW
RESET_COLOR   = Fore.RESET

def scheme_chars():
    return ['G', 'Y', 'R',\
            'g', 'y', 'r',\
            'D', 'K']

def highlight(x):
    return {
        'K': COLOR_CYAN,
        'D': COLOR_CYAN,
        'R': COLOR_RED,
        'Y': COLOR_YELLOW,
        'G': COLOR_GREEN,
        'r': COLOR_RED,
        'y': COLOR_YELLOW,
        'g': COLOR_GREEN,
    }[x]

def scheme():
    pic  = "        | D |        \n"
    pic += "      K | r | K      \n"
    pic += "________| y |________\n"
    pic += "          g          \n"
    pic += "      RYG   GYR      \n"
    pic += "________  g  ________\n"
    pic += "        | y |        \n"
    pic += "      K | r | K      \n"
    pic += "        | D |        "
    return pic

def not_allowed_input(c):
    return c not in ['0', '2', '4', '6',
                     'b', 's', 'bs', 'sb',
                     'k', 'd', 'kd', 'dk',
                     '']

def input_letters_dict(c):
    dict_ = {'sb': 6, 'bs': 6, 'b': 4, 's': 2,
             'dk': 6, 'kd': 6, 'k': 4, 'd': 2,
               '': 0}
    return c if c not in dict_ else dict_[c]

def tt_check_line(line):
    line = line.replace(' ', '')
    line = line.replace('9', '-')
    line = line[:-1]

    allowed_trans = []
    for i in range(STATES_NUM):
        allowed_trans.append(str(i))
    allowed_trans.append('-')

    for c in line:
        if c not in allowed_trans:
            exit_(STR_WRONG_TRANSITION)

    return line

signal_chars = ['K', 'D', 'Z']

#===================================================================================

ARG_BYSTEP = '--by_step'

INPUT_QUIT = 'q'

FORMAT_COMSEQ_IND = '{:>03d}'
FORMAT_COMSEQ     = '{:>03b}'
FORMAT_CFG_L_COL  = '{:03b} | '
FORMAT_TICK_L_COL = '{:02d} | '

STR_EMPTY_COMMSEQ     = COLOR_RED + "Empty command sequence!"                    + RESET_COLOR
STR_WRONG_TRANS_TABLE = COLOR_RED + "Inappropriate transitions table!"           + RESET_COLOR
STR_WRONG_TRANSITION  = COLOR_RED + "Transition table contains wrong statement!" + RESET_COLOR
STR_WRONG_INPUT       = COLOR_RED + "Wrong input!"                               + RESET_COLOR

def print_by_step_help():
    STR_HELP          = "\n"
    STR_HELP         += "Control signals:\n"
    STR_HELP         += "b = button, s = sensor,\n"
    STR_HELP         += "sb = button and sensor,\n"
    STR_HELP         += "Enter = skip, q = quit"
    print(STR_HELP)

STR_ENTER_CTRL        = "Enter control signal: "

STR_HDR_SCHEME        = "Scheme:"
STR_HDR_COMMSEQ       = "Command sequence:"
STR_HDR_TRTABLE       = "Transitions:"
STR_HDR_OUTPUT        = "Output:"

STR_TRTABLE_HEAD      = COLOR_MAGENTA + "C\S" + RESET_COLOR + " | "
STR_OUTTABLE_HEAD     = "TI | COM K D Z O O"

STR_END               = '\n' + "The prediction is over."
