#!/usr/bin/env python

#title             : TA Lab 2
#module            : finite state machine Moore type
#python_version    : 3.4
#required_packages : colorama 0.3

class moore_state_machine(object):
    def __init__(self, cfg):
        self._output_map = cfg['outputs'] if 'outputs' in cfg else {}
        self._trans_map = cfg['transitions'] if 'transitions' in cfg else [[]]
        self._current_state = None if len(self._trans_map) == 0 else self._trans_map[0][0]

    def get_current_state(self):
        return self._current_state

    def change_state(self, control_signal):
        if self._trans_map[control_signal][int(self._current_state)] == '-':
            raise Exception("Can't be here!")

        self._current_state = self._trans_map[control_signal][int(self._current_state)]
        return self.get_output()

    def get_output(self):
        return self._output_map['{0:03b}'.format(int(self._current_state))]

    def reset(self):
        self._current_state = self._trans_map[0][0]
