k   = .4;
T   = 2.3;
T1  = T;
T2  = 3;
ksi = .5;

set(0,'DefaultAxesXGrid','on','DefaultAxesYGrid','on');

w = [0:.1:10];
W_aper = k./(T.*1i.*w + 1);
plot (w, W_aper);

figure;
U_aper = k./((T.*w).^2 + 1);
plot (w, U_aper);

figure;
V_aper = k.*T.*w./((T.*w).^2 + 1);
plot (w, V_aper);

figure;
A_aper = k./(sqrt((T.*w).^2 + 1));
plot (w, A_aper);

figure;
phi_aper = -atan(T.*w);
plot (w, phi_aper);

figure;
L_aper = 20.*log10(k./(sqrt((T.*w).^2 + 1)));
plot (w, L_aper);

figure;

w = [1/T:.1:100];
L_aper_A2 = 20.*log10(k) - 20.*log10(T.*w);
plot (log10(w), L_aper_A2);
hold on
line([-1 log10(1/T)],[20*log10(k) 20*log10(k)])

figure;
sys_aper = tf([k], [T 1])
bodeplot(sys_aper)

%==================================================

W_kol = k./((1 - (T.*w).^2) + 1i.*2.*ksi.*T.*w)
U_kol = k.*(1 - (T.*w).^2)./((1 - (T.*w).^2).^2 + (2.*ksi.*T.*w).^2)
V_kol = -2.*k.*ksi.*T.*w./((1 - (T.*w).^2).^2 + (2.*ksi.*T.*w).^2)
phi_kol_1 = -atan(2.*ksi.*T.*w./(1 - (T.*w).^2))
phi_kol_1 = -pi - atan(2.*ksi.*T.*w./(1 - (T.*w).^2))
A_kol = k./(sqrt((1 - (T.*w).^2).^2 + (2.*ksi.*T.*w).^2))
L_kol = 20.*log10(k) - 20.*log10(sqrt((1 - (T.*w).^2).^2 + (2.*ksi.*T.*w).^2))

sys_kol = tf([k],[-T.^2 1i.*2.*ksi.*T 1])

%==================================================

sys = tf([k], [T 1])
ltiview('step', sys)
1+4.6 p+10.58 p^2+12.167 p^3


sys = tf([k],[12.167 10.58 4.6 1])
ltiview('step', sys)