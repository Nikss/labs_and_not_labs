#!/usr/bin/env python

# title           : TA Lab 3
# module          : expression state machine
# python_version  : 3.4

from absm import *
from term import *

class expression(absm):
    def __init__(self, cfg, term):
        super().__init__(cfg)

        self._term = term
        self._action = '+'
        self._result = .0

    def get_result(self):
        return self._result

    def process(self, char):
        if super().get_state() == 0:
            self._term.reset()
            self._action = '+'
            self._result = .0

        if char.isalpha() or char.isdigit(): 
            signal = 0
        else:
            signs = {'+': 1, 
                     '-': 2, 
                     ';': 4
            }
            for s in ['*', '/', '.']: signs[s] = 3
            signal = signs[char] if char in signs else 5

        super().change_state(signal)

        def send_term(c):
            self._term.process(c)

        def fin_state(c):
            self._term.process(';')

            if self._action == '+':
                self._result += self._term.get_result()
            elif self._action == '-':
                self._result -= self._term.get_result()

        def input_plus(c):
            fin_state(c)
            self._action = '+'
            self._term.reset()

        def input_minus(c): 
            fin_state(c)
            self._action = '-'
            self._term.reset()

        def err(c):
            raise ErrorState(self.__class__.__name__)

        actions = {
            1: send_term,
            2: input_plus,
            3: input_minus,
            4: fin_state,
            5: err
        }

        actions[super().get_state()](char)
