#!/usr/bin/env python

# title           : TA Lab 3
# module          : term state machine
# python_version  : 3.4

from absm import *
from opnd import operand
from cfg_ import STR_ERROR_ZERODIV

class term(absm):
    def __init__(self, cfg, operand):
        super().__init__(cfg)

        self._opnd = operand
        self._action = '*'
        self._result = 1.

    def get_result(self):
        return self._result

    def process(self, char):
        if super().get_state() == 0:
            self._opnd.reset()
            self._result = 1.
            self._action = '*'

        if char.isalpha() or char.isdigit(): 
            signal = 0
        else:
            signs = {'*': 1,
                     '/': 2,
                     '.': 3,
                     ';': 4
                    }
            signal = signs[char] if char in signs else 5

        super().change_state(signal)

        def send_oper(c): 
            self._opnd.process(c)

        def fin_state(c): 
            self._opnd.process(';')

            if self._action == '*':
                self._result *= self._opnd.get_result()
            elif self._action == '/':
                try:
                    self._result /= self._opnd.get_result()
                except ZeroDivisionError:
                    print(STR_ERROR_ZERODIV)
                    exit(1)

        def input_ast(c):
            fin_state(c)
            self._action = '*'
            self._opnd.reset()  

        def input_slash(c): 
            fin_state(c)
            self._action = '/'
            self._opnd.reset()

        def err(c):
            raise ErrorState(self.__class__.__name__)

        actions = {
            1: send_oper,
            2: input_ast,
            3: input_slash,
            4: fin_state,
            5: err
        }

        actions[super().get_state()](char)
