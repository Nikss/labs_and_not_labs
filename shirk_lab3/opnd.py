#!/usr/bin/env python

# title           : TA Lab 3
# module          : operand state machine
# python_version  : 3.4

from absm import absm
from cfg_ import STR_DICT_ERROR

class operand(absm):
    def __init__(self, cfg, dictionary):
        super().__init__(cfg)

        self._dict = dictionary

        self._digit_weight = 1.
        self._result = 0.
        self._varname = ''

    def get_result(self):
        return self._result

    def process(self, char):
        if super().get_state() == 0:
            self._digit_weight = 1.
            self._result = 0.
            self._varname = ''

        signal = 3
        if   char.isalpha(): signal = 0
        elif char.isdigit(): signal = 1
        elif char == '.':    signal = 2

        super().change_state(signal)

        def add_to_name(c):
            self._varname += c

        def proc_integ(c):
            self._result *= 10
            self._result += float(c)

        def prep_fract(c): pass
        def read_fract(c):
            self._digit_weight /= 10
            self._result += float(c)*self._digit_weight

        def fin_var(c):
            try:
                self._result = self._dict[self._varname]
            except KeyError:
                print(STR_DICT_ERROR % self._varname)
                exit(1)

        def num_done(c): pass

        def err(c):
            raise ErrorState(self.__class__.__name__)

        actions = {
            1: add_to_name,
            2: proc_integ,
            3: prep_fract,
            4: read_fract,
            5: fin_var,
            6: num_done,
            7: err
        }

        actions[super().get_state()](char)
