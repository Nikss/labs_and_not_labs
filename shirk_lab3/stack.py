#!/usr/bin/env python

# title           : TA Lab 3
# module          : stack memory state machines
# python_version  : 3.4

from absm import *
from expr import expression

class f_stack(absm):
    def __init__(self, cfg):
        super().__init__(cfg)

        self._2nd = None
        self._stack = []

    def link_2nd(self, s_stack): 
        self._2nd = s_stack

    def pop(self):
        return self._stack.pop()

    def push(self, char):
        if super().get_state() == 0:
            self._2nd.reset()
            self._stack = []
        
        signal = 1 if char == ')' or char == ';' else 0
        
        super().change_state(signal)

        def stack_push(c):
            self._stack.append(c)

        def stack_spinup(c):
            self._2nd.reset()
            while self._stack and\
              self._2nd.get_state() != 2:
                self._2nd.push(self.pop())

        actions = {
            1: stack_push,
            2: stack_spinup,
        }

        actions[super().get_state()](char)

class s_stack(absm):
    def __init__(self, cfg, expression):
        super().__init__(cfg)
        self._1st = None
        self._expr = expression
        self._stack = []

    def link_1st(self, f_stack): 
        self._1st = f_stack

    def pop(self):
        return self._stack.pop()

    def push(self, char):
        if super().get_state() == 0:
            self._expr.reset()
            self._stack = []
        
        signal = 1 if char == '(' or char == '=' else 0

        super().change_state(signal)

        def stack_push(c):
            self._stack.append(c)

        def stack_spinup(c):
            while self._stack:
                self._expr.process(self.pop())
            self._expr.process(';')
            
            for d in str(self._expr.get_result()):
                self._1st.push(d)

        actions = {
            1: stack_push,
            2: stack_spinup,
        }

        actions[super().get_state()](char)
