#!/usr/bin/env python

# title           : TA Lab 3
# module          : operator state machine
# python_version  : 3.4

from absm import *
from stack import f_stack
from expr import expression

class operator(absm):
    def __init__(self, cfg, f_stack, expression, dictionary):
        super().__init__(cfg)

        self._expr = expression
        self._stack = f_stack
        self._dict = dictionary
        self._newvar = ''

    def process(self, char):
        if super().get_state() == 0:
            self._stack.reset()
            self._newvar = ''

        if char.isalpha(): signal = 0
        elif char.isdigit(): signal = 1
        else:
            signs = {'(': 2,
                     ')': 3, 
                     '=': 4,
                     ';': 5
                    }
            for c_ in ['*', '/', '+', '-', '.']: signs[c_] = 6
            signal = signs[char] if char in signs else 7

        super().change_state(signal)

        def add_to_name(c): 
            self._newvar += c

        def push_char(c): 
            self._stack.push(c)

        def fin_var(c): 
            self._stack.push(';')
            self._dict[self._newvar]\
                = self._expr.get_result()

        def err(c):
            raise ErrorState(self.__class__.__name__)

        actions = {
            1: add_to_name,
            2: push_char,
            3: push_char,
            4: fin_var,
            5: err
        }

        actions[super().get_state()](char)
