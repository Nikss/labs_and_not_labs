#!/usr/bin/env python

# title            : TA Lab 3
# module           : constants
# python_version   : 3.4

STR_INPUT            = '\n' + 'Input:' + '\n'
STR_DICT_ERROR       = "\'Operand\' s.m.: var \'%s\' is not in the dictionary!"
STR_ERROR_STATE      = "\'%s\' s.m. has entered the error state!"
STR_WRONG_TRANS      = "Wrong transition in \'%s\' s.m.!"
STR_WRONG_TRANS_INFO = "Attempted to access [row %d, col %d] in the transition table."
STR_ERROR_ZERODIV    = "Error in \'Term\' s.m.: division by 0!"
STR_RESULT           = "Result: %s"
STR_END              = '\n' + "The prediction is over."

opnd_cfg = [[1, 1, 7, 7, 7, 5, 6, 7],
            [2, 1, 2, 4, 4, 5, 6, 7],
            [7, 7, 3, 7, 7, 5, 6, 7],
            [7, 5, 6, 7, 6, 5, 6, 7]]

term_cfg = [[1, 1, 1, 1, 4, 5],
            [5, 2, 5, 5, 4, 5],
            [5, 3, 5, 5, 4, 5],
            [5, 1, 5, 5, 4, 5],
            [5, 4, 5, 5, 4, 5],
            [5, 5, 5, 5, 5, 5]]

expr_cfg = [[1, 1, 1, 1, 4, 5],
            [5, 2, 5, 5, 4, 5],
            [5, 3, 5, 5, 4, 5],
            [5, 1, 5, 5, 4, 5],
            [5, 4, 5, 5, 4, 5],
            [5, 5, 5, 5, 5, 5]]

stack_cfg = [[1, 1, 1],
             [2, 2, 2]]

opor_cfg = [[1, 1, 3, 3, 4, 5],
            [5, 1, 3, 3, 4, 5],
            [5, 5, 3, 3, 4, 5],
            [5, 5, 5, 3, 4, 5],
            [5, 2, 5, 5, 4, 5],
            [5, 5, 5, 4, 4, 5],
            [5, 5, 5, 3, 4, 5],
            [5, 5, 5, 5, 2, 5]]

trans_cfg = [[0, 0, 3],
             [1, 3, 3],
             [3, 2, 3]]
