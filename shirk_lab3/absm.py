#!/usr/bin/env python

# title           : TA Lab 3
# module          : state machine, base class
# python_version  : 3.4

class ErrorState(Exception):
    def __init__(self, sm_name):
        self.sm_name = sm_name.title()

class WrongTransition(Exception):
    def __init__(self, sm_name, signal, state):
        self.sm_name = sm_name.title()
        self.signal = signal
        self.state = state

class absm:
    def __init__(self, cfg):
        self._tt = cfg
        self._state = 0

    def get_state(self):
        return self._state

    def change_state(self, signal):
        try:
            self._state = self._tt[signal][self._state]
        except IndexError:
            raise WrongTransition(self.__class__.__name__,\
                signal, self._state)

    def reset(self):
        self._state = 0
