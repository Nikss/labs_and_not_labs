#!/usr/bin/env python

# title           : TA Lab 3
# module          : entry point
# python_version  : 3.4
# date            : 03.2015

from sys import argv

from absm  import ErrorState, WrongTransition
from trans import translator
from cfg_  import *

def main():
    the_machine = translator(opnd_cfg, term_cfg, expr_cfg,\
        stack_cfg, opor_cfg, trans_cfg)

    with open(argv[1]) as f:
        print(STR_INPUT)
        print(f.read()); f.seek(0)

        while not the_machine.was_stopped():
            char = f.read(1)
            if not char: break
            if char == '\n': continue

            try:
                the_machine.process(char)
                
            except ErrorState as e:
                print(STR_ERROR_STATE % e.sm_name)
                exit(1)

            except WrongTransition as e:
                print(STR_WRONG_TRANS % e.sm_name)
                print(STR_WRONG_TRANS_INFO % (e.signal, e.state))
                exit(1)

    print(STR_END)

main()
