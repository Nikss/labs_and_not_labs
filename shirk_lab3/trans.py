#!/usr/bin/env python

# title           : TA Lab 3
# module          : translator state machine (top level)
# python_version  : 3.4

from absm  import *
from opnd  import operand
from term  import term
from expr  import expression
from stack import f_stack, s_stack
from opor  import operator
from cfg_  import STR_RESULT

class translator(absm):
    def __init__(self, opnd_cfg, term_cfg, expr_cfg,\
        stack_cfg, opor_cfg, cfg):
        super().__init__(cfg)

        self._dict = {}

        self._opnd = operand(opnd_cfg, self._dict)
        self._term = term(term_cfg, self._opnd)
        self._expr = expression(expr_cfg, self._term)

        self._f_stack = f_stack(stack_cfg)
        self._s_stack = s_stack(stack_cfg, self._expr)

        self._f_stack.link_2nd(self._s_stack)
        self._s_stack.link_1st(self._f_stack)

        self._opor = operator(opor_cfg, self._f_stack,\
            self._expr, self._dict)

        self._stopped = False

        self.reset()

    def process(self, char):
        signal = 0
        if char == ';': signal = 1
        if char == '#': signal = 2

        super().change_state(signal)

        def send_to_operator(c): 
            self._opor.process(c)

        def fin_operator(c): 
            self._opor.process(c)
            self._opor.reset()

        def done(c):
            print(STR_RESULT % self._expr.get_result())
            self._stopped = True

        actions = {
            0: send_to_operator,
            1: fin_operator,
            2: done
        }

        actions[super().get_state()](char)

    def reset(self):
        self._stopped = False

        self._dict = {}

        for sm in [super(),
                   self._opnd,
                   self._term,
                   self._expr,
                   self._f_stack,
                   self._s_stack,
                   self._opor
                  ]: sm.reset()

    def was_stopped(self):
        return self._stopped
